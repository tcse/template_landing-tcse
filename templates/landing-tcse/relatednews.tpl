<li class="list-group-item">
	<a href="{link}">
		{title}
		<span class="pull-right"><i class="fa fa-angle-double-right"></i></span>
	</a>
</li>