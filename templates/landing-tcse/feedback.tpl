{* <div class="row"> *}
	<div class="col-sm-9">
		<h1>Обратная связь</h1>
		<ul class="list-group">
			[not-logged]
			<li class="list-group-item"><input placeholder="Ваше имя" type="text" maxlength="35" name="name" class="form-control"></li>
			<li class="list-group-item"><input placeholder="E-mail" type="email" maxlength="35" name="email" class="form-control"></li>
			[/not-logged]

			<li class="list-group-item"><input placeholder="Заголовок" type="text" maxlength="45" name="subject" class="form-control"><div style="display: none">{recipient}</div></li>
			<li class="list-group-item"><textarea placeholder="Сообщение" name="message" row="3" class="form-control"></textarea></li>
			
			[sec_code]
			<li class="list-group-item">
				<div class="c-captcha-box">
					<label for="sec_code">Повторите код:</label>
					<div class="c-captcha">
						<p>{code}</p>
						<p><input title="Введите код указанный на картинке" type="text" name="sec_code" id="sec_code" class="form-control" placeholder="сюда вводите код"></p>
					</div>
				</div>
			</li>
			[/sec_code]

			[recaptcha]
			<li class="list-group-item">
				<div>Введите слова</div>
				{recaptcha}
			</li>
			[/recaptcha]

			[question]
			<li class="list-group-item">
				<div><b>Вопрос:</b></div>
				<p>{question}</p>
			</li>

			<li class="list-group-item">
				<div><b>Ответ:</b> <span class="impot">*</span></div>
				<input type="text" name="question_answer" id="question_answer" placeholder="укажите ответ на вопрос" class="form-control" />
			</li>
			[/question]

		</ul>
		<div class="submitline">
			<button name="submit" class="btn btn-success" type="submit">Отправить</button>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
		<p><br></p>
	</div>
{* </div> *}