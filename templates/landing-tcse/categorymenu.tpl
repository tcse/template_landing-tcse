[root]<ul class="nav navbar-nav">[/root]
	[item]
		<li[isparent] class="dropdown"[/isparent]>
			<a href="{url}"[isparent] class="dropdown-toggle" data-toggle="dropdown" {* data-target="{url}" *}[/isparent]>
				{name} 
				<sup>({news-count})</sup>
				[isparent]<b class="caret"></b>[/isparent]
			</a>
			[sub-prefix]<ul class="dropdown-menu">[/sub-prefix]
				{sub-item}
			[sub-suffix]</ul>[/sub-suffix]
		</li>
	[/item]
[root]</ul>[/root]
